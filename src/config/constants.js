export const items = [
  {
    title: 'Home',
    href: '/',
  },
  {
    title: 'About',
    href: '/about-us',
  },
  {
    dropdown: true,
    title: 'Services',
    items: [
      {
        name: 'Service One',
        href: '/service-one',
      },
      {
        name: 'Service Two',
        href: '/service-two',
      },
    ],
  },
  {
    title: 'Galery',
    href: '/galery',
  },
  {
    title: 'Blog',
    href: '/blog',
  },
  {
    title: 'Contact',
    href: '/contact-us',
  },
];
